#include "pcproject.h"
// Define custom functions used across multiple files.

// Run a loop.
void run_quantum(int x){
	volatile int i;
	for(; x; x--){
		for(i = 0; i < 1000000; i++);
	}
	return;
}

//Compare start time. Used for sorting.
int cmp_time(const void *a, const void *b){
	process_info **A = (process_info**)a;
	process_info **B = (process_info**)b;
	return (int)((*A)->start_time - (*B)->start_time);
}

//Compare exec time. Used for sorting.
int cmp_runningTime(const void *a, const void *b){
	process_info **A = (process_info**)a;
	process_info **B = (process_info**)b;
	return (int)((*A)->running_time - (*B)->running_time);
}
