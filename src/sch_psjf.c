#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <signal.h>
#define __USE_GNU
#include <sched.h>
#include <unistd.h>
#include "pcproject.h"
#include "temp_sys_calls.h"

// Scheduler function for the PSJF scheme.
void sch_psjf(process_info **proc_ptr, const int N,
			const struct timespec quantum){
	//puts("Hi! I am FIFO");
	int i;
	int current_time = 0;
	struct timespec sleeptime;
	struct sched_param schPSJF;
	cpu_set_t cpuSET;
	long long qtns = ((long long)quantum.tv_sec)*1000000000 + quantum.tv_nsec;

	CPU_ZERO(&cpuSET);
	CPU_SET(0, &cpuSET);

	//Assigning priorities.
	qsort(proc_ptr, N, sizeof(process_info*), cmp_runningTime);
	for(i = 0; i < N; i++)
	{
		proc_ptr[i] -> priority = 98 - i;
	}
	qsort(proc_ptr, N, sizeof(process_info*), cmp_time); //Sort according to start time.
	/*for(i = 0; i < N; i++){
		printf("%s %d %d\n", 	proc_ptr[i] -> name,
								proc_ptr[i] -> start_time,
								proc_ptr[i] -> running_time);
	}

	printf("%d %d\n", quantum.tv_sec, quantum.tv_nsec);*/ //debug
	//Start up processes one by one.
	for(i = 0; i < N; i++){
		long long sleeptemp = qtns*(proc_ptr[i] -> start_time - current_time);
		
		//Sleep until next process.
		sleeptime.tv_sec = (int)(sleeptemp/1000000000);
		sleeptime.tv_nsec = (int)(sleeptemp%1000000000);
		nanosleep(&sleeptime, NULL);
		current_time = proc_ptr[i] -> start_time;

		//Forking.
		if((proc_ptr[i] -> pid = fork()) > 0){
			// Do nothing. below are debugging tools.
			//sched_setscheduler(proc_ptr[i] -> pid, SCHED_FIFO, NULL);
			//printf("%d %d\n", currentProc, i);
		} else if(proc_ptr[i] -> pid == 0){
			struct timespec start, finish;
			
			printf("%s %d\n", proc_ptr[i] -> name, getpid());
			mynstimeofday(&start);
			
			//Assign priority to one self.
			schPSJF.sched_priority = proc_ptr[i] -> priority;
			if(sched_setscheduler(getpid(), SCHED_FIFO, &schPSJF) != 0){
				fprintf(stderr, "setscheduler failed.\n");
			}
			//Assign to CPU 0
			if(sched_setaffinity(getpid(), sizeof(cpu_set_t), &cpuSET) < 0){
				fprintf(stderr, "set affinity failed.\n");
			}
			//Run
			run_quantum(proc_ptr[i] -> running_time);

			//Finishing cleanup.
			mynstimeofday(&finish);
			log_dmesg("[Project1]", getpid(), start, finish);
			exit(0);
		} else {
			fputs("fork error", stderr);
		}
	}
	//Wait for processes.
	for(i = 0; i < N; i++){
		wait(NULL);
	}
	return;
}
