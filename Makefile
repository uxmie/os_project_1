CXX = gcc
CXXFLAGS = -Wall -Iinclude
SRC = src
SCHEDULER_OBJ = custom_functions.o temp_sys_calls.o sch_fifo.o sch_rr.o sch_sjf.o sch_psjf.o scheduler.o
PROCESS_OBJ = process.o custom_functions.o
RM = rm -rf
MODPATH = src
MODULENAME = schmods
obj-m += $(MODPATH)/$(MODULENAME).o

.PHONY: run clean all noOBJ
all:  module scheduler

noOBJ: scheduler
	-$(RM) *.o

#process: $(PROCESS_OBJ)
#	$(CXX) $(CXXFLAGS) $(PROCESS_OBJ) -o $@

scheduler: $(SCHEDULER_OBJ)
	$(CXX) $(CXXFLAGS) $(SCHEDULER_OBJ) -o $@

module:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

%.o: $(SRC)/%.c
	$(CXX) $(CXXFLAGS) -c $<

clean:
	-$(RM) process scheduler *.o
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

